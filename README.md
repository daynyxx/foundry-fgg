<!-- ALL-CONTRIBUTORS-BADGE:START - Do not remove or modify this section -->
[![All Contributors](https://img.shields.io/badge/all_contributors-16-orange.svg?style=flat-square)](#contributors-)
<!-- ALL-CONTRIBUTORS-BADGE:END -->

## Installation

Find this Foundry VTT game system within Foundry VTT's system browser with the search term `fgg`.

You may also copy the manifest link below and paste it into the manifest link input box.

```console
TODO: MAKE GITLAB RUNNER WORK
```

## Your Support

If you use this system and want to show your appreciation, here's a couple free things you can do.

- If you're a Gitlab user, please give us a star.
- If you speak JavaScript, please consider [contributing code](/CONTRIBUTING.md).

## Troubleshooting and Requesting User Support

It's always appreciated when users [submit an issue](https://gitlab.com/daynyxx/foundry-fgg/-/issues/new) for all kinds of user questions, comments, and concerns. For those who prefer support via public chat, we recommend asking in:

- The **#dnd-other** channel in the [official Foundry VTT Discord server](https://discord.gg/foundryvtt)
- The [/r/FoundryVTT community on Reddit](https://reddit.com/r/FoundryVTT) may also have answers to your Foundry VTT questions, but it is another general Foundry VTT user community that may not have many users with specific knowledge about OSE. Be sure to write `[OSE]` in your post subject and select `FVTT Question` from the "Flair" menu.

## Contributions

Any feedback is deeply appreciated. Please browse the [open issues](https://gitlab.com/daynyxx/foundry-fgg/-/issues) and if there's not a matching one already, open a new one.

If you're a developer, look for our [CONTRIBUTING.md](/CONTRIBUTING.md) file.

## Open Game Content Used Under License

See [LICENSE.OGL](/LICENSE.OGL) file.

## Trademarks Used Under License

<!-- END TEXT REQUIRED BY LICENSE -->

See [LICENSE.OTHER.OSE_THIRD_PARTY_V1_5](/LICENSE.OTHER.OSETHIRDPARTY.V1_5) file.

## Code Used Under License

See [LICENSE.GPL](/LICENSE.GPL) file.

## Artwork Used Under License

Weapon quality icons, and the Treasure chest are from [Rexxard](https://assetstore.unity.com/packages/2d/gui/icons/flat-skills-icons-82713).

## Licensing Inquiries

You may direct licensing inquiries to [dev@daynyxx.com](mailto:dev@daynyxx.com).

## Contributors ✨

Thanks goes to these wonderful people ([emoji key](https://allcontributors.org/docs/en/emoji-key)) who contributed to the OSE project which this was forked from:

<!-- ALL-CONTRIBUTORS-LIST:START - Do not remove or modify this section -->
<!-- prettier-ignore-start -->
<!-- markdownlint-disable -->
<table>
  <tbody>
    <tr>
      <td align="center" valign="top" width="14.28%"><a href="https://github.com/Godforsaken84"><img src="https://avatars.githubusercontent.com/u/100036544?v=4?s=100" width="100px;" alt="Godforsaken84"/><br /><sub><b>Godforsaken84</b></sub></a><br /><a href="#design-Godforsaken84" title="Design">🎨</a></td>
      <td align="center" valign="top" width="14.28%"><a href="https://github.com/freohr"><img src="https://avatars.githubusercontent.com/u/3462951?v=4?s=100" width="100px;" alt="Stephen FAURE"/><br /><sub><b>Stephen FAURE</b></sub></a><br /><a href="https://github.com/vttred/ose/commits?author=freohr" title="Code">💻</a></td>
      <td align="center" valign="top" width="14.28%"><a href="https://github.com/engleback"><img src="https://avatars.githubusercontent.com/u/35422051?v=4?s=100" width="100px;" alt="Ian Engleback"/><br /><sub><b>Ian Engleback</b></sub></a><br /><a href="https://github.com/vttred/ose/commits?author=engleback" title="Code">💻</a></td>
      <td align="center" valign="top" width="14.28%"><a href="https://github.com/RabidOwlbear"><img src="https://avatars.githubusercontent.com/u/71675732?v=4?s=100" width="100px;" alt="Grim"/><br /><sub><b>Grim</b></sub></a><br /><a href="https://github.com/vttred/ose/commits?author=RabidOwlbear" title="Code">💻</a></td>
      <td align="center" valign="top" width="14.28%"><a href="https://github.com/WallaceMcGregor"><img src="https://avatars.githubusercontent.com/u/17795541?v=4?s=100" width="100px;" alt="WallaceMcGregor"/><br /><sub><b>WallaceMcGregor</b></sub></a><br /><a href="#translation-WallaceMcGregor" title="Translation">🌍</a></td>
      <td align="center" valign="top" width="14.28%"><a href="https://github.com/IGALEGOI"><img src="https://avatars.githubusercontent.com/u/97805442?v=4?s=100" width="100px;" alt="IGALEGOI"/><br /><sub><b>IGALEGOI</b></sub></a><br /><a href="#translation-IGALEGOI" title="Translation">🌍</a></td>
      <td align="center" valign="top" width="14.28%"><a href="https://github.com/sanviler"><img src="https://avatars.githubusercontent.com/u/96877404?v=4?s=100" width="100px;" alt="Lorenzo Castelletta"/><br /><sub><b>Lorenzo Castelletta</b></sub></a><br /><a href="#translation-sanviler" title="Translation">🌍</a></td>
    </tr>
    <tr>
      <td align="center" valign="top" width="14.28%"><a href="https://github.com/HerrSquash"><img src="https://avatars.githubusercontent.com/u/97633309?v=4?s=100" width="100px;" alt="herrsquash"/><br /><sub><b>herrsquash</b></sub></a><br /><a href="#translation-herrsquash" title="Translation">🌍</a></td>
      <td align="center" valign="top" width="14.28%"><a href="http://linktr.ee/teuri"><img src="https://avatars.githubusercontent.com/u/64547748?v=4?s=100" width="100px;" alt="Igor Teuri"/><br /><sub><b>Igor Teuri</b></sub></a><br /><a href="#translation-igorteuri" title="Translation">🌍</a></td>
      <td align="center" valign="top" width="14.28%"><a href="https://github.com/hogwrassler"><img src="https://avatars.githubusercontent.com/u/110945935?v=4?s=100" width="100px;" alt="hogwrassler"/><br /><sub><b>hogwrassler</b></sub></a><br /><a href="https://github.com/vttred/ose/commits?author=hogwrassler" title="Code">💻</a></td>
      <td align="center" valign="top" width="14.28%"><a href="http://timsnyder.codes"><img src="https://avatars.githubusercontent.com/u/1731267?v=4?s=100" width="100px;" alt="Tim"/><br /><sub><b>Tim</b></sub></a><br /><a href="https://github.com/vttred/ose/commits?author=wyrmisis" title="Code">💻</a></td>
      <td align="center" valign="top" width="14.28%"><a href="https://github.com/maschill92"><img src="https://avatars.githubusercontent.com/u/4692066?v=4?s=100" width="100px;" alt="Michael Schilling"/><br /><sub><b>Michael Schilling</b></sub></a><br /><a href="https://github.com/vttred/ose/commits?author=maschill92" title="Code">💻</a></td>
      <td align="center" valign="top" width="14.28%"><a href="http://adamoresten.com"><img src="https://avatars.githubusercontent.com/u/12858387?v=4?s=100" width="100px;" alt="Adam Oresten"/><br /><sub><b>Adam Oresten</b></sub></a><br /><a href="https://github.com/vttred/ose/commits?author=Haxxer" title="Code">💻</a></td>
      <td align="center" valign="top" width="14.28%"><a href="https://github.com/bakbakbakbakbak"><img src="https://avatars.githubusercontent.com/u/105067023?v=4?s=100" width="100px;" alt="bakbakbakbakbak"/><br /><sub><b>bakbakbakbakbak</b></sub></a><br /><a href="https://github.com/vttred/ose/commits?author=bakbakbakbakbak" title="Code">💻</a></td>
    </tr>
    <tr>
      <td align="center" valign="top" width="14.28%"><a href="https://github.com/adn770"><img src="https://avatars.githubusercontent.com/u/113786?v=4?s=100" width="100px;" alt="Josep Torra"/><br /><sub><b>Josep Torra</b></sub></a><br /><a href="#translation-adn770" title="Translation">🌍</a></td>
      <td align="center" valign="top" width="14.28%"><a href="https://github.com/amir-arad"><img src="https://avatars.githubusercontent.com/u/6019373?v=4?s=100" width="100px;" alt="Amir Arad"/><br /><sub><b>Amir Arad</b></sub></a><br /><a href="https://github.com/vttred/ose/commits?author=amir-arad" title="Code">💻</a></td>
    </tr>
  </tbody>
</table>

<!-- markdownlint-restore -->
<!-- prettier-ignore-end -->

<!-- ALL-CONTRIBUTORS-LIST:END -->

This project follows the [all-contributors](https://github.com/all-contributors/all-contributors) specification. Contributions of any kind welcome!
